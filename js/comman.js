$(document).ready(function(){

  /*===========================
  =            Nav            =
  ===========================*/
    window.slide = new SlideNav();

    $(function() {
      var header = $(".ds-navbar");
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 50) {
          header.addClass("scrolled");
        } else {
          header.removeClass("scrolled");
        }
      });
    });
  /*=====  End of Nav  ======*/

  /*=================================
  =            Animation            =
  =================================*/
    AOS.init();
  /*=====  End of Animation  ======*/
  
  /*===============================
  =            Sliders            =
  ===============================*/
  
  if($('.ds-main-slider').length){
    $('.ds-main-slider').slick({
      speed: 250,
      arrows: false,
      autoplay: true,
      dots: false
    });  
  }

  if($('.ds-industry-detail-slider').length){
    $('.ds-industry-detail-slider').slick({
      speed: 250,
      arrows: true,
      autoplay: true,
      prevArrow:'<span class="ad-arrow-left"><i class="ad-icon-caret-left-thin"></i></span>',
      nextArrow:'<span class="ad-arrow-right"><i class="ad-icon-caret-right-thin"></i></span>',
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: false,
      responsive: [
      {
        breakpoint: 1300,
        settings: {
          arrows: false,
        }
      },
      {
        breakpoint: 991,
        settings: {
          arrows: false,
          slidesToShow: 2
        }
      },
      {
        breakpoint: 767,
        settings: {
          arrows: false,
          slidesToShow: 1
        }
      }]
    });
  }
  
  /*=====  End of Sliders  ======*/
  

  /*============================================
    =            Custom Select Picker            =
    ============================================*/
    
    if($('.ds-select').length){
      $('.ds-select').selectpicker();      
    }
    
    /*=====  End of Custom Select Picker  ======*/
    
})