App = {
  web3Provider: null,
  contracts: {},
  isConnected: false,

  init: async function() {
    
    await App.bindEvents();
    return await App.initWeb3();
  },

  initWeb3: async function() {
    
    if(window.ethereum){
      App.web3Provider = window.ethereum;
      try{
        await window.ethereum.enable();
        $('#connect-button').html("Connected");
        App.isConnected = true;
      } catch (error){
        console.error("User denied account access");
        window.alert("Please Allow Access")
      }
    }else if(window.web3) {
      App.web3Provider = window.web3.currentProvider;
    }else{
      App.web3Provider = new web3.providers.HttpProvider('http://localhost:7545');
    }

    web3 = new Web3(App.web3Provider);


    return App.initContract();
  },

  initContract: function() {
    
    $.getJSON('Transaction.json', function(data) {

      var TransacionArtifact = data;
      //console.log(TransacionArtifact);
      App.contracts.Transaction = TruffleContract(TransacionArtifact);

      App.contracts.Transaction.setProvider(App.web3Provider);

      //$('#connect-button').html("Connected");
      // return App.markAdopted();
    })

    // return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-drop', App.handleAirDrop);
    $(document).on('click', '.btn-buy', App.BuyTok);
    $(document).on('click', '.wall-connect', App.initWeb3);
  },


  handleAirDrop: async function(event) {
    event.preventDefault();

    // if(typeof(web3.eth) == 'undefined'){
    //   window.alert("Please Connect Wallet First.");
    // } 
    
    var TransactionInstance;
    try{
      web3.eth.getAccounts(function(error, accounts){
      if(error){
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.Transaction.deployed().then(function(instance) {
        TransactionInstance = instance;
        
        // return adoptionInstance.adoption(petId, {from: account});
        return TransactionInstance.getAirDrop({from: account, value: '652248496241092', gas: '125028', gasPrice: '5000000000'});
      }).catch(function(err) {
        if(err.message == 'invalid address'){
          $('#connect-button').html("Connect Wallet");
          window.alert("Please Connect Wallet First.")
          console.log(err.message);
        }else{
          console.log(err.message);
        }
      });
      })
    }catch(err){
      if(err.message == "Cannot read property 'getAccounts' of undefined"){
        $('#connect-button').html("Connect Wallet");
        window.alert("Please Connect Wallet First.")
      }else{
      console.log(err.message);
      }
    }
    
  },

  BuyTok: async function(event){
    event.preventDefault();

    if(typeof(web3.eth) == 'undefined'){
      window.alert("Please Connect Wallet First.");
    }

    var TransactionInstance;
    try{
      web3.eth.getAccounts(function(error, accounts){

        if(error){
          console.log(error);
        }

        var account = accounts[0]
        var amount = $('input').val();
        console.log(amount);
        if(amount.split('.').length > 2 || amount <= 0){
          window.alert("Please enter correct Amount");
        }else if(Number(amount) < 0.006){
          window.alert("Minimum buying amount is 0.006")
        }
        else{

          amount = amount*(10**18);
          App.contracts.Transaction.deployed().then(function(instance){

            TransactionInstance = instance;
            return TransactionInstance.buyToken({from: account, value: String(amount), gas: '125028', gasPrice: '5000000000'});
          }).catch(function(err) {
            if(err.message == 'invalid address'){
              $('#connect-button').html("Connect Wallet");
              window.alert("Please Connect Wallet First.");
            }else{
              console.log(err.message);
            }
          });

        }
           
      })
    }catch(err){
      if(err.message == "Cannot read property 'getAccounts' of undefined"){
        $('#connect-button').html("Connect Wallet");
        window.alert("Please Connect Wallet First.")
      }else{
      console.log(err.message);
      }
    } ;
  },

  AddTok: async function(event){
    //event.preventDefault();

    const tokenAddress = '0xDA3Dd0c6333887A03351409e03f4aBD4c5D04d61';
    const tokenSymbol = 'DSB';
    const tokenDecimals = 10;
    const tokenImage = 'https://www.diamondshiba.com/images/wallet_logo.jpeg';

    try {
      // wasAdded is a boolean. Like any RPC method, an error may be thrown.
      const wasAdded = await ethereum.request({
        method: 'wallet_watchAsset',
        params: {
          type: 'ERC20', // Initially only supports ERC20, but eventually more!
          options: {
            address: tokenAddress, // The address that the token is at.
            symbol: tokenSymbol, // A ticker symbol or shorthand, up to 5 chars.
            decimals: tokenDecimals, // The number of decimals in the token
            image: tokenImage, // A string url of the token logo
          },
        },
      });

      if (wasAdded) {
        console.log('Thanks for your interest!');
      } else {
        console.log('Your loss!');
      }
    } catch (error) {
      console.log(error);
    }
  },


};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
