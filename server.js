const express = require('express');


const app = express();


app.use(express.static(".")); 


app.get('/', function(req, res){

    res.sendFile('index.html');
})


let port = process.env.PORT;
if (port == null || port == "") {
  port = 3000;
}
app.listen(port, function(){
	console.log("server has started")
});